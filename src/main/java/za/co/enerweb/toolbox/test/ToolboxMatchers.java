package za.co.enerweb.toolbox.test;

public abstract class ToolboxMatchers {

    public static RegexMatcher like(String regex) {
        return new RegexMatcher(regex);
    }
}
