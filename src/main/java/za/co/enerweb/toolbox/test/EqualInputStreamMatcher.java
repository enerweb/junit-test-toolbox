package za.co.enerweb.toolbox.test;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import za.co.enerweb.toolbox.string.StringBuilderUtil;

/**
 * This matcher tels you exactly where in the stream things went wrong..
 */
public class EqualInputStreamMatcher extends
    BaseMatcher<InputStream> {
    private InputStream expected;
    private static final int EOF = -1;
    private StringBuilderUtil description = new StringBuilderUtil();
    boolean matching = true;
    private boolean bufferStreams;

    public EqualInputStreamMatcher(InputStream expected, boolean bufferStreams) {
        this.expected = expected;
        this.bufferStreams = bufferStreams;
    }

    @Override
    public boolean matches(Object actual) {
        if (actual instanceof InputStream) {
            InputStream actualIs  = (InputStream) actual;

            if (bufferStreams) {
                if (!(expected instanceof BufferedInputStream)) {
                    expected = new BufferedInputStream(expected);
                }
                if (!(actualIs instanceof BufferedInputStream)) {
                    actualIs = new BufferedInputStream(actualIs);
                }
            }
            long count = 0;
            try {
                int expectedChar = expected.read();
                while (EOF != expectedChar) {
                    int actualChar = actualIs.read();
                    if (expectedChar != actualChar) {
                        addMessage("Actual=" + actualChar
                            + " doesn't match expected=" +
                            expectedChar
                            + " at byte " + count + " .");
                    }
                    count++;
                    expectedChar = expected.read();
                }

                int actualChar = actualIs.read();
                if (actualChar != EOF) {
                    addMessage("Actual doesn't end at the same "
                        + "byte as expected (" + count + ").");
                }
            } catch (IOException e) {
                addMessage(e.getMessage());
            }
        } else {
            addMessage("Actual is not an InputStream.");
        }
        return matching;
    }

    private void addMessage(String msg) {
        description.addLine(
            msg);
        matching = false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(this.description.toString());
    }

    public static BaseMatcher<InputStream> isEqualInputStream(
        final InputStream expected) {
        return new EqualInputStreamMatcher(expected, true);
    }

    public static BaseMatcher<InputStream> isEqualInputStreamNoBuffer(
        final InputStream expected) {
        return new EqualInputStreamMatcher(expected, false);
    }
}
