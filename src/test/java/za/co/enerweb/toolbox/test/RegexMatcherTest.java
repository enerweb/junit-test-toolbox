package za.co.enerweb.toolbox.test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static za.co.enerweb.toolbox.test.ToolboxMatchers.like;

import org.junit.Test;

public class RegexMatcherTest {

    private static final String TEXT = "abc123def";

    @Test
    public void test() {
        assertThat(TEXT, like(".*c1.*"));
        assertThat(TEXT, like(".*c\\d+d.*"));
        assertThat(TEXT, not(like(".*c2.*")));
    }

}
